# Node_treater

Small piece of code to calculate node and cluster specificity for each herb in a molecular network of several herbs.
Each line of the code is commented to make it usable for anyone.

Minimal requirment : a feature table with intesity/area for each herb of the mixture and the mixture itself
AND the GNPS componentindex column (ie the *'ID'* of each cluster in the Molecular Network).

If you used the FBMN worflow on GNPS, you juste need your job's URL or task-ID.

The code will provide you a table with:

- the fetaures specific to a plant of you mixture

- the componeintindex of the clusters specific (according to your criteria) to each herb as well as some basic visualization plots. Makes it easier to spot these specific cluster and gives a metric for the cluster specificity .

- a MS1 2D map of the specific features

Ref: to be published